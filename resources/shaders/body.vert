#version 140
in vec3 pos;      // vertex position
in vec3 normal;
in vec3 world_pos; // world position
in float scale;

out vec3 frag_color;

uniform mat4 perspective, model;

void main()
{
    vec4 vertex = model*vec4(((scale*pos)+world_pos), 1.0);

    frag_color = pos;

    gl_Position = perspective*vertex;
}
