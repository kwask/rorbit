use std::{thread, time};
use std::fs::{File, create_dir};
use std::path::Path;
use std::io::{Write, Read};
use std::error::Error;
#[allow(unused_imports)] use log::{error, warn, info, debug, trace};

pub mod data;
use data::Data;

pub mod system;
use system::System; // needed to use SystemManager.process()
use system::SystemManager;

pub mod types;

#[derive(PartialEq, Copy, Clone)]
enum State
{
    Start,
    Run,
    Stop,
    Exit,
    Closed
}

pub struct Engine
{
    state:          State,
    system_manager: SystemManager,
    data:           Data,
}

impl Engine
{
    pub fn new(
        app_name:       &'static str,
        userdata_dir:   &str,
        resource_dir:   &str,
    ) -> Self
    {
        simple_logger::init().unwrap();
        info!("{} written by Nick Hirzel, 2019", app_name);
        info!("initializing");

        let mut data: Data = match create_dir(userdata_dir.clone())
        {
            Ok(()) => 
            {
                info!(
                    "creating {:?} directory",
                    userdata_dir
                );

                Data::new(
                    app_name,
                    Engine::version(),
                    userdata_dir,
                    resource_dir,
                )
            },
            Err(_) => 
            {
                let file_path = userdata_dir.to_string()+&data::STATE_FILE.to_string();

                match File::open(&Path::new(&file_path))
                {
                    Ok(mut file) => 
                    {
                        let mut contents = String::new();
                        match file.read_to_string(&mut contents)
                        {
                            Ok(_) => match serde_json::from_str(&contents)
                            {
                                Ok(loaded_data) => 
                                {
                                    info!("state loaded from {:?}", file_path);
                                    loaded_data
                                },
                                Err(why) =>
                                {
                                    error!(
                                        "file {:?} is in invalid form:\n{}",
                                        file_path,
                                        why
                                    );
                                    Data::new(
                                        app_name,
                                        Engine::version(),
                                        userdata_dir,
                                        resource_dir,
                                    )
                                },
                            },
                            Err(_) => 
                            {
                                error!(
                                    "couldn't read from state file {:?}",
                                    file_path
                                );
                                Data::new(
                                    app_name,
                                    Engine::version(),
                                    userdata_dir,
                                    resource_dir,
                                )
                            }
                        }
                    },
                    Err(_) => 
                    {
                        info!(
                            "no existing state file {}, creating new state",
                            file_path
                        );
                        Data::new(
                            app_name,
                            Engine::version(),
                            userdata_dir,
                            resource_dir,
                        )
                    }
                }
            }
        };

        data.version.update(Engine::version());

        let mut engine = Engine
        {
            state:          State::Start,
            system_manager: SystemManager::default(),
            data:           data,
        };

        // processes the current state
        match engine.system_manager.init(&mut engine.data)
        {
            Err(error) =>
            {
                error!("{:?}", error);
                engine.state = State::Exit;
            }
            _          => ()
        }

        return engine;
    }
    
    pub fn transition(&mut self)
    {
        match self.state
        {
            State::Start    => self.start(),
            State::Run      => self.run(),
            State::Stop     => self.stop(),
            State::Exit     => self.exit(),
            _               => (),
        }
    }

    pub fn start(&mut self)
    {   
        info!("starting");

        self.state = State::Run;
    }
    
    pub fn run(&mut self)
    {
        info!("running");

        'run: loop
        {
            let start_time = time::Instant::now();

            if self.data.should_exit
            {
                break 'run;
            }

            // processes the current state
            match self.system_manager.process(&mut self.data)
            {
                Err(error) =>
                {
                    error!("{:?}", error);
                    break 'run;
                }
                _          => ()
            }

            // frame-limiting
            let elapsed_time = time::Instant::now()-start_time;
            let sleep_time = time::Duration::new(
                0,
                self.data.display.framerate.ns_per_frame
            );

            if sleep_time > elapsed_time
            {
                thread::sleep(sleep_time-elapsed_time);
            }
        }

        // if the loop has stopped, it means the program is exiting
        self.state = State::Stop;
    }

    pub fn stop(&mut self)
    {
        info!("stopping");
        self.state = State::Exit;
    }

    pub fn exit(&mut self)
    {
        info!("exiting");

        match self.save_data()
        {
            Err(err) => error!("{}", err),
            _        => (),
        }

        self.state = State::Closed;
    }

    pub fn exited(&self) -> bool
    {
        return self.state == State::Closed;
    }

    fn save_data(&self) -> Result<(), String>
    {
        let userdata = self.data.userdata_dir.clone();
        let state_file = data::STATE_FILE;

        info!("saving state...");
        match create_dir(userdata.clone())
        {
            Ok(())   => info!("created {:?} directory", userdata),
            Err(_)   => (), // directory already existed
        }

        let file_path = userdata+state_file;

        let mut file = match File::create(&Path::new(&file_path))
        {
            Ok(file) => file,
            Err(err) => return Err(format!(
                "couldn't create file {:?}: {}",
                file_path,
                err.description()
            )),
        };

        match file.write_all(
            serde_json::to_string_pretty(&self.data).unwrap()
                .into_bytes()
                .as_slice()
        )
        {
            Ok(_) => info!("state saved to {:?}", file_path),
            Err(err) => return Err(format!(
                "couldn't write to file {:?}: {}",
                file_path,
                err.description()
            )),
        }

        Ok(())
    }

    fn version() -> &'static str
    {
        env!("CARGO_PKG_VERSION")
    }
}
