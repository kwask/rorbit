use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq)]
pub struct Mapper<T>
{
    pub objects:   Vec<T>,
    pub map:       HashMap<String, usize>,
}

impl<T> Mapper<T>
{
    pub fn new() -> Self
    {
        Self
        {
            objects:    Vec::new(),
            map:        HashMap::new(),
        }
    }

    pub fn contains_key(&self, name: &str) -> bool
    {
        self.map.contains_key(name)
    }

    pub fn insert(&mut self, name: &str, object: T)
    {
        self.map.insert(name.to_string(), self.objects.len());
        self.objects.push(object);
    }

    pub fn id(&self, name: &str) -> usize
    {
        self.map[name]
    }

    pub fn get(&self, id: usize) -> &T
    {
        &self.objects[id]
    }
}
