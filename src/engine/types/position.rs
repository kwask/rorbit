use std::ops::{Add, AddAssign, Sub, SubAssign};
use glut::dpi::LogicalPosition;
use serde::{Serialize, Deserialize};

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct Position
{
    pub x: f32,
    pub y: f32,
}

impl Position
{
    pub fn new(x: f32, y: f32) -> Self
    {
        Self
        {
            x: x,
            y: y
        }
    }
}

impl Add for Position
{
    type Output = Position;

    fn add(self, other: Position) -> Position
    {
        Position::new(self.x+other.x, self.y+other.y)
    }
}

impl AddAssign for Position
{
    fn add_assign(&mut self, other: Self)
    {
        *self = (*self)+other;
    }
}

impl Sub for Position
{
    type Output = Position;

    fn sub(self, other: Self) -> Position
    {
        Position::new(self.x-other.x, self.y-other.y)
    }
}

impl SubAssign for Position
{
    fn sub_assign(&mut self, other: Self)
    {
        *self = (*self)-other;
    }
}

impl From<LogicalPosition> for Position
{
    fn from(position: LogicalPosition) -> Self
    {
        Self::new(position.x as f32, position.y as f32)
    }
}

impl From<(f32, f32)> for Position
{
    fn from((x, y): (f32, f32)) -> Self
    {
        Self::new(x, y)
    }
}

impl From<(f64, f64)> for Position
{
    fn from((x, y): (f64, f64)) -> Self
    {
        Self::new(x as f32, y as f32)
    }
}

impl From<(i32, i32)> for Position
{
    #[inline]
    fn from((x, y): (i32, i32)) -> Self
    {
        Self::new(x as f32, y as f32)
    }
}

impl Into<LogicalPosition> for Position
{
    fn into(self) -> LogicalPosition
    {
        LogicalPosition::new(self.x.into(), self.y.into())
    }
}

// manually define i32 Into to properly round numbers
impl Into<(i32, i32)> for Position
{
    fn into(self) -> (i32, i32)
    {
        (self.x.round() as i32, self.y.round() as i32)
    }
}
