use serde::{Serialize, Deserialize};
use glut::dpi::LogicalSize;

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct Area
{
    pub length: f32,
    pub height: f32,
}

impl Area
{
    pub fn new(length: f32, height: f32) -> Self
    {
        Self
        {
            length: length,
            height: height
        }
    }
}

impl From<LogicalSize> for Area
{
    fn from(size: LogicalSize) -> Self
    {
        Self::new(size.width as f32, size.height as f32)
    }
}

impl From<(f32, f32)> for Area
{
    fn from((length, height): (f32, f32)) -> Self
    {
        Self::new(length, height)
    }
}

impl From<(f64, f64)> for Area
{
    fn from((length, height): (f64, f64)) -> Self
    {
        Self::new(length as f32, height as f32)
    }
}

impl From<(i32, i32)> for Area
{
    #[inline]
    fn from((length, height): (i32, i32)) -> Self
    {
        Self::new(length as f32, height as f32)
    }
}

impl Into<LogicalSize> for Area
{
    fn into(self) -> LogicalSize
    {
        LogicalSize::new(self.length.into(), self.height.into())
    }
}

// manually define i32 Into to properly round numbers
impl Into<(i32, i32)> for Area
{
    fn into(self) -> (i32, i32)
    {
        (self.length.round() as i32, self.height.round() as i32)
    }
}

