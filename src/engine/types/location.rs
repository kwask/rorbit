use serde::{Serialize, Deserialize};

use crate::engine::types::{Position, Area};

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct Location
{
    position:   Position,
    area:       Area,
}

impl Location
{
    pub fn new(position: Position, area: Area) -> Self
    {
        Self
        {
            position:   position,
            area:       area,
        }
    }

    pub fn contains(&self, position: &Position) -> bool
    {
        if position.x < self.position.x
        {
            return false;
        }

        if position.y < self.position.y
        {
            return false;
        }

        if position.x > self.position.x+self.area.length
        {
            return false;
        }

        if position.y > self.position.y+self.area.height
        {
            return false;
        }

        return true;
    }
}


