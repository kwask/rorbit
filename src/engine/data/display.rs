#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use std::f32::consts::PI;
use std::borrow::Borrow;
use std::fmt;
use glut::{EventsLoop, WindowBuilder, ContextBuilder};
use gl::{Program};
use gl::Display as GlDisplay;
use gl_text::{TextSystem, FontTexture};
use serde::{Serialize, Deserialize};
use serde::ser::{Serializer, SerializeStruct};
use serde::de::{self, Deserializer, Visitor, SeqAccess, MapAccess};

use crate::engine::types::{Mapper, Area};

pub mod mesh;
mod model;
pub use model::Model;

mod meshbuffer;
pub use meshbuffer::MeshBuffer;

mod color;
pub use color::Color;

mod camera;
pub use camera::Camera;

mod framerate;
pub use framerate::Framerate;

pub const SHADER_DIR: &str = "shaders/";
pub const FONT_DIR: &str = "fonts/";

#[derive(Debug, Serialize, Deserialize)]
pub enum DisplayMode
{
    POINTS,
    LINES,
    PLANES,
    TEXTURES
}

// Stores all data necessary for rendering
pub struct Display
{
    pub name:           String,
    pub display:        GlDisplay,
    pub event_pump:     EventsLoop,
    pub text_system:    TextSystem,
    pub display_mode:   DisplayMode,
    pub framerate:      Framerate,
    pub dimensions:     Area,
    pub clear_color:    Color,
    pub camera:         Camera,
    pub shaders:        Mapper<Program>,
    pub fonts:          Mapper<FontTexture>,
    pub mesh_buffers:   Vec<MeshBuffer>,
}

impl Display
{
    pub fn new(
        name:           &str,
        dimensions:     Area,
        fov:            f32,
        framerate:      f32,
        resizable:      bool,
        target_id:      usize,
    ) -> Self
    {
        info!("creating event pump");
        let event_pump = EventsLoop::new();

        info!("creating display with new OpenGL context");
        let window = WindowBuilder::new()
            .with_title(name)
            .with_dimensions(dimensions.into())
            .with_resizable(resizable)
            .into();

        let context_builder = ContextBuilder::new()
            .with_depth_buffer(24);

        let gl_display = GlDisplay::new(
            window, 
            context_builder,
            &event_pump
        ).unwrap();
        
        info!("creating text system");
        let text_system = TextSystem::new(&gl_display);

        Self
        {
            name:           name.to_string(),
            display:        gl_display,
            event_pump:     event_pump,
            text_system:    text_system,
            display_mode:   DisplayMode::PLANES,
            framerate:      Framerate::new(framerate),
            dimensions:     dimensions,
            clear_color:    Color::rgb(0.0, 0.0, 0.0),
            camera:         Camera::new(dimensions, fov, target_id),
            shaders:        Mapper::new(),
            fonts:          Mapper::new(),
            mesh_buffers:   Vec::new(),
       }
    }

    pub fn recreate(
        name:           &str,
        display_mode:   DisplayMode,
        framerate:      &Framerate,
        dimensions:     Area,
        clear_color:    &Color,
        camera:         &Camera,
    ) -> Self
    {
        let mut display = Display::new(
            name,
            dimensions,
            camera.fov,
            framerate.frames_per_s,
            true,
            camera.target_id,
        );

        display.camera = *camera;
        display.display_mode = display_mode;
        display.clear_color = *clear_color;

        return display;
    }

    pub fn cycle_display_mode(&mut self)
    {
        match self.display_mode
        {
            DisplayMode::POINTS => self.display_mode = DisplayMode::LINES,
            DisplayMode::LINES => self.display_mode = DisplayMode::PLANES,
            DisplayMode::PLANES => self.display_mode = DisplayMode::TEXTURES,
            DisplayMode::TEXTURES => self.display_mode = DisplayMode::POINTS,
        }
    }

    pub fn resize_window(&mut self, dimensions: Area)
    {
        self.dimensions = dimensions;
        self.camera.set_aspect_ratio(dimensions);
        self.display
            .gl_window()
            .borrow()
            .window()
            .set_inner_size(self.dimensions.into());
    }

    pub fn capture_mouse(&mut self, capture: bool)
    {
        self.display
            .gl_window()
            .borrow()
            .window()
            .grab_cursor(capture).unwrap();
        self.display
            .gl_window()
            .borrow()
            .window()
            .hide_cursor(capture);
    }
}

impl Default for Display
{
    fn default() -> Self
    {
        Display::new(
            env!("CARGO_PKG_NAME"),
            Area::new(1440.0, 900.0),
            PI/4.0,
            60.0,
            true,
            0
        )
    }
}

impl Serialize for Display
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let fields = 6; // the number of fields to serialize in the struct
        let mut state = serializer.serialize_struct("Display", fields)?;
        state.serialize_field("name", &self.name)?;
        state.serialize_field("display_mode", &self.display_mode)?;
        state.serialize_field("framerate", &self.framerate)?;
        state.serialize_field("dimensions", &self.dimensions)?;
        state.serialize_field("clear_color", &self.clear_color)?;
        state.serialize_field("camera", &self.camera)?;
        state.end()
    }
}

impl<'de> Deserialize<'de> for Display
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[allow(non_camel_case_types)]
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "lowercase")]
        enum Field { name, display_mode, framerate, dimensions, clear_color, camera };

        struct DisplayVisitor;

        impl<'de> Visitor<'de> for DisplayVisitor
        {
            type Value = Display;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result
            {
                formatter.write_str("struct Display")
            }

            // this is used by sequential formats, line bincode
            fn visit_seq<V>(self, mut seq: V) -> Result<Display, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let name = seq.next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                let display_mode = seq.next_element()?
                    .ok_or_else(|| de::Error::invalid_length(1, &self))?;
                let framerate: Framerate = seq.next_element()?
                    .ok_or_else(|| de::Error::invalid_length(2, &self))?;
                let dimensions = seq.next_element()?
                    .ok_or_else(|| de::Error::invalid_length(3, &self))?;
                let clear_color: Color = seq.next_element()?
                    .ok_or_else(|| de::Error::invalid_length(4, &self))?;
                let camera: Camera = seq.next_element()?
                    .ok_or_else(|| de::Error::invalid_length(5, &self))?;

                Ok(Display::recreate(
                    name,
                    display_mode,
                    &framerate,
                    dimensions,
                    &clear_color,
                    &camera,
                ))
            }

            // this is used by mapped formats, like JSON
            fn visit_map<V>(self, mut map: V) -> Result<Display, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut name = None;
                let mut display_mode = None;
                let mut framerate = None;
                let mut dimensions = None;
                let mut clear_color = None;
                let mut camera = None;

                while let Some(key) = map.next_key()?
                {
                    match key
                    {
                        Field::name =>
                        {
                            if name.is_some()
                            {
                                return Err(de::Error::duplicate_field("name"));
                            }
                            name = Some(map.next_value()?);
                        }
                        Field::display_mode =>
                        {
                            if display_mode.is_some()
                            {
                                return Err(de::Error::duplicate_field("display_mode"));
                            }
                            display_mode = Some(map.next_value()?);
                        }
                        Field::framerate =>
                        {
                            if framerate.is_some()
                            {
                                return Err(de::Error::duplicate_field("framerate"));
                            }
                            framerate = Some(map.next_value()?);
                        }
                        Field::dimensions =>
                        {
                            if dimensions.is_some()
                            {
                                return Err(de::Error::duplicate_field("dimensions"));
                            }
                            dimensions = Some(map.next_value()?);
                        }
                        Field::clear_color =>
                        {
                            if clear_color.is_some()
                            {
                                return Err(de::Error::duplicate_field("clear_color"));
                            }
                            clear_color = Some(map.next_value()?);
                        }
                        Field::camera =>
                        {
                            if camera.is_some()
                            {
                                return Err(de::Error::duplicate_field("camera"));
                            }
                            camera = Some(map.next_value()?);
                        }
                    }
                }

                let name = name.ok_or_else(
                    || de::Error::missing_field("name")
                )?;
                let display_mode = display_mode.ok_or_else(
                    || de::Error::missing_field("display_mode")
                )?;
                let framerate: Framerate = framerate.ok_or_else(
                    || de::Error::missing_field("framerate")
                )?;
                let dimensions = dimensions.ok_or_else(
                    || de::Error::missing_field("dimensions")
                )?;
                let clear_color: Color = clear_color.ok_or_else(
                    || de::Error::missing_field("clear_color")
                )?;
                let camera: Camera = camera.ok_or_else(
                    || de::Error::missing_field("camera")
                )?;

                Ok(Display::recreate(
                    name,
                    display_mode,
                    &framerate,
                    dimensions,
                    &clear_color,
                    &camera,
                ))
            }
        }

        const FIELDS: &'static [&'static str] = &[
            "name",
            "display_mode",
            "framerate",
            "dimensions",
            "clear_color",
            "camera"
        ];
        deserializer.deserialize_struct("Display", FIELDS, DisplayVisitor)
    }
}

impl fmt::Debug for Display
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        write!(
            f,
            "{{ display_mode {:?}, framerate: {:?}, window_dim: {:?}, camera: {:?} }}",
            self.display_mode,
            self.framerate,
            self.dimensions,
            self.camera,
        )
    }
}

