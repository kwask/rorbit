use crate::engine::types::{Area, Position};

// Commands are one-time events, they don't persist
#[derive(Debug, Copy, Clone)]
pub enum Command
{
    WindowResize(Area),
    MoveCursor(Position),
    Zoom(f32),
    CycleRenderMode,
    NextTarget,
    PreviousTarget,
    GrabCamera,
    UngrabCamera,
    TogglePause,
    Exit,
}
