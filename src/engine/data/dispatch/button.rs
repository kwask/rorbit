use serde::{Serialize, Deserialize};
use glut::{VirtualKeyCode, MouseButton};

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum Button
{
    Key(VirtualKeyCode),
    Mouse(MouseButton)
}
