// Signals are continuous events, they'll persist until ended
// Signals must be hashable, so you cannot pass any f32 or f64 values
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub enum Signal
{
    Move(i8, i8, i8),
    Rotate(i8, i8),
    CameraGrabbed,
}
