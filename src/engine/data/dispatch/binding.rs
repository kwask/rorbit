use serde::{Serialize};
use serde::ser::{Serializer, SerializeStruct};

use crate::engine::data::dispatch::{command::Command, signal::Signal};

#[derive(Debug, Clone)]
pub struct Binding
{
    pub name:       String,
    pub start:      Option<Command>,
    pub stop:       Option<Command>,
    pub hold:       Option<Signal>,
}

impl Binding
{
    pub fn new(start: Command, stop: Command, hold: Signal) -> Self
    {
        Self
        {
            name:       "unnamed".to_string(),
            start:      Some(start),
            stop:       Some(stop),
            hold:       Some(hold),
        }
    }

    pub fn start(start: Command) -> Self
    {
        Self
        {
            name:       "unnamed".to_string(),
            start:      Some(start),
            stop:       None,
            hold:       None,
        }
    }

    pub fn start_hold(start: Command, hold: Signal) -> Self
    {
        Self
        {
            name:       "unnamed".to_string(),
            start:      Some(start),
            stop:       None,
            hold:       Some(hold),
        }
    }

    pub fn hold(hold: Signal) -> Self
    {
        Self
        {
            name:       "unnamed".to_string(),
            start:      None,
            stop:       None,
            hold:       Some(hold),
        }
    }
}

impl Serialize for Binding
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let fields = 1; // the number of fields to serialize in the struct
        let mut state = serializer.serialize_struct("Binding", fields)?;
        state.serialize_field("name", &self.name)?;
        state.end()
    }
}
