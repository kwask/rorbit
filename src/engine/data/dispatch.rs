use std::vec::Vec;
use std::collections::{HashMap, HashSet};
use glut::{VirtualKeyCode, MouseButton};
use serde::{Serialize, Deserialize};
use serde::ser::{Serializer, SerializeMap};
use serde::de::{Deserializer, Visitor, MapAccess};
use std::fmt;

use crate::engine::types::Position;

mod command;
pub use command::Command;

mod signal;
pub use signal::Signal;

mod binding;
pub use binding::Binding;

mod button;
pub use button::Button;

#[derive(Debug, Clone)]
pub struct Dispatch
{
    pub binding:            HashMap<String, Binding>, // binding names -> binding
    pub mapping:            HashMap<Button, Binding>, // button press -> binding
    pub commands:           Vec<Command>,
    pub signals:            HashSet<Signal>,
    pub cursor:             Position,
    pub cursor_velocity:    Position,
}

impl Dispatch
{
    pub fn new() -> Self
    {
        let mut dispatch = Dispatch
        {
            binding:            HashMap::new(),
            mapping:            HashMap::new(),
            commands:           Vec::new(),
            signals:            HashSet::new(),
            cursor:             Position::new(0.0, 0.0),
            cursor_velocity:    Position::new(0.0, 0.0),
        };
        dispatch.default_binding();
        return dispatch;
    }

    pub fn default_mapping(&mut self)
    {
        self.mapping = HashMap::new();

        use Button::{Key, Mouse};

        // Key mappings
        use VirtualKeyCode::*;
        self.map(Key(W), "move forward");
        self.map(Key(S), "move backward");
        self.map(Key(A), "move left");
        self.map(Key(D), "move right");
        self.map(Key(Q), "yaw left");
        self.map(Key(E), "yaw right");
        self.map(Key(R), "pitch up");
        self.map(Key(F), "pitch down");
        self.map(Key(Equals), "zoom in");
        self.map(Key(Subtract), "zoom out");
        self.map(Key(Tab), "cycle render mode");
        self.map(Key(Comma), "previous target");
        self.map(Key(Period), "next target");
        self.map(Key(Space), "toggle pause");
        self.map(Key(Escape), "exit");

        // Mouse bindings
        use MouseButton::*;
        self.map(Mouse(Middle), "grab camera");
    }

    pub fn default_binding(&mut self)
    {
        self.binding = HashMap::new();

        self.bind("move forward", Binding::hold(Signal::Move(1, 0, 0)));
        self.bind("move backward", Binding::hold(Signal::Move(-1, 0, 0)));
        self.bind("move left", Binding::hold(Signal::Move(0, -1, 0)));
        self.bind("move right", Binding::hold(Signal::Move(0, 1, 0)));
        self.bind("yaw left", Binding::hold(Signal::Rotate(-1, 0)));
        self.bind("yaw right", Binding::hold(Signal::Rotate(1, 0)));
        self.bind("pitch up", Binding::hold(Signal::Rotate(0, -1)));
        self.bind("pitch down", Binding::hold(Signal::Rotate(0, 1)));
        self.bind("zoom in", Binding::start(Command::Zoom(-10.0)));
        self.bind("zoom out", Binding::start(Command::Zoom(10.0)));
        self.bind("cycle render mode", Binding::start(Command::CycleRenderMode));
        self.bind("previous target", Binding::start(Command::PreviousTarget));
        self.bind("next target", Binding::start(Command::NextTarget));
        self.bind("toggle pause", Binding::start(Command::TogglePause));
        self.bind("exit", Binding::start(Command::Exit));
        self.bind("grab camera", Binding::new(
            Command::GrabCamera,
            Command::UngrabCamera,
            Signal::CameraGrabbed
        ));
    }

    pub fn bind(&mut self, name: &str, mut bind: Binding)
    {
        bind.name = name.to_string();
        self.binding.insert(name.to_string(), bind);
    }

    pub fn map(&mut self, button: Button, bind_name: &str)
    {
        self.mapping.insert(button, self.binding[bind_name].clone());
    }

    pub fn queue(&mut self, command: Command)
    {
        self.commands.push(command);
    }

    pub fn press(&mut self, button: &Button)
    {
        match self.mapping.get(button)
        {
            Some(binding) => 
            {
                match binding.start
                {
                    Some(command) => self.commands.push(command.clone()),
                    None => ()
                }

                match binding.hold
                {
                    Some(signal) => { self.signal(signal); },
                    None => ()
                }
            },
            None => ()
        }
    }

    pub fn release(&mut self, button: &Button)
    {
        match self.mapping.get(button)
        {
            Some(binding) => 
            {
                match binding.stop
                {
                    Some(command) => self.commands.push(command.clone()),
                    None => ()
                }

                match binding.hold
                {
                    Some(signal) => { self.extinguish(&signal); },
                    None => ()
                }
            },
            None => ()
        }
    }

    pub fn signal(&mut self, signal: Signal) -> bool
    {
        self.signals.insert(signal)
    }

    pub fn extinguish(&mut self, signal: &Signal) -> bool
    {
        self.signals.remove(signal)
    }
}

impl Default for Dispatch
{
    fn default() -> Self
    {
        let mut dispatch = Dispatch::new();
        dispatch.default_mapping();
        return dispatch
    }
}

impl Serialize for Dispatch
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(self.mapping.len()))?;
        
        for (k, v) in &self.mapping
        {
            map.serialize_entry(&v.name, k)?;
        }

        map.end()
    }
}

struct DispatchVisitor;

impl<'de> Visitor<'de> for DispatchVisitor
{
    type Value = Dispatch;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result
    {
        formatter.write_str("struct Dispatch")
    }

    // this is used by mapped formats, like JSON
    fn visit_map<V>(self, mut map: V) -> Result<Dispatch, V::Error>
    where
        V: MapAccess<'de>,
    {
        let mut mapping: HashMap<String, Button> = HashMap::with_capacity(map.size_hint().unwrap_or(0));

        // While there are entries remaining in the input, add them
        // into our map.
        while let Some((key, value)) = map.next_entry()?
        {
            mapping.insert(key, value);
        }
        
        let mut dispatch = Dispatch::new();
        for (bind_name, button) in mapping
        {
            dispatch.mapping.insert(button, dispatch.binding[&bind_name].clone());
        }

        Ok(dispatch)
    }
}

impl<'de> Deserialize<'de> for Dispatch
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        const FIELDS: &'static [&'static str] = &[
            "mapping",
        ];

        deserializer.deserialize_struct("Dispatch", FIELDS, DispatchVisitor)
    }
}
