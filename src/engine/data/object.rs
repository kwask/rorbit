use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Object
{
    pub name:       String,
    pub scale:      f32,
    pub model_id:   usize,
}

impl Object
{
    pub fn new(
        name:       &str,
        scale:      f32,
        model_id:   usize,
    ) -> Self
    {
        Self
        {
            
            name:       name.to_string(),
            scale:      scale,
            model_id:   model_id,
        }
    }
}
