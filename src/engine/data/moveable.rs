use nalgebra::{Vector3, Point3};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub struct Moveable
{
    pub position:   Point3<f32>,
    pub velocity:   Vector3<f32>,
}

impl Moveable
{
    pub fn new(
        position:   &Point3<f32>,
        velocity:   &Vector3<f32>,
    ) -> Self
    {
        Self
        {
            
            position:   *position,
            velocity:   *velocity,
        }
    }
}
