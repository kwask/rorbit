use gl::{VertexBuffer, IndexBuffer};

use crate::engine::data::display::mesh::{Vertex, Normal, Indice};

pub struct MeshBuffer
{
    pub vertices:   VertexBuffer<Vertex>,
    pub indices:    IndexBuffer<Indice>,
    pub normals:    VertexBuffer<Normal>,
    pub shader:     usize
}

impl MeshBuffer
{
    pub fn new(
        vertices:   VertexBuffer<Vertex>,
        indices:    IndexBuffer<Indice>,
        normals:    VertexBuffer<Normal>,
        shader:     usize
    ) -> Self
    {
        Self
        {
            vertices: vertices,
            indices: indices,
            normals: normals,
            shader: shader,
        }
    }
}
