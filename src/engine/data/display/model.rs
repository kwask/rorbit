use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
pub struct Model
{
    pub mesh:           usize,
}

impl Model
{
    pub fn new(
        mesh: usize,
    ) -> Self
    {
        Self
        {
            mesh:           mesh,
        }
    } 
}
