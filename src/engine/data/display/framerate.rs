use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Framerate
{
    pub frames_per_s:   f32,
    pub s_per_frame:    f32,
    pub ns_per_frame:   u32,
}

impl Framerate
{
    pub fn new(framerate: f32) -> Self
    {
        let mut fr = Framerate
        {
            frames_per_s:   0.0,
            s_per_frame:    0.0,
            ns_per_frame:   0,
        };

        fr.set_framerate(framerate);
        return fr;
    }

    pub fn set_framerate(&mut self, framerate: f32)
    {
        self.frames_per_s = framerate;
        self.s_per_frame = 1.0/self.frames_per_s;
        self.ns_per_frame = (self.s_per_frame*1_000_000_000.0) as u32; // nanoseconds
    }
}
