use std::collections::HashMap;
#[allow(unused_imports)] use log::{error, warn, info, debug, trace};

use serde::{Serialize, Deserialize};

mod vertex;
pub use vertex::Vertex;

mod normal;
pub use normal::Normal;

pub type Indice = u16;
pub type Vertices = Vec<vertex::Vertex>;
pub type Indices = Vec<Indice>;
pub type Normals = Vec<normal::Normal>;

#[derive(Debug, Serialize, Deserialize)]
pub struct Mesh
{
    pub vertices:       Vertices,
    pub indices:        Indices,
    pub normals:        Normals,
    pub shader_name:    String,
}

const VERTS_PER_FACE: usize = 3;

impl Mesh
{
    pub fn new() -> Self
    {
        Self
        {
            vertices:       Vec::new(),
            indices:        Vec::new(),
            normals:        Vec::new(),
            shader_name:    String::new(),
        }
    }

    pub fn faces(&self) -> usize
    {
        self.indices.len()/3
    }

    pub fn create(
        vertices: Vertices,
        indices: Indices,
        normals: Normals,
        shader_name: &str
    ) -> Self
    {
        Self
        {
            vertices:       vertices,
            indices:        indices,
            normals:        normals,
            shader_name:    shader_name.to_string(),
        }
    }

    fn calc_normals(vertices: &Vertices, indices: &Indices) -> Normals
    {
        let mut normals: Normals = Vec::new();

        for i in (0..indices.len()).step_by(VERTS_PER_FACE)
        {
            // we calculate a plane's normal from three vertices within the plane
            let v0 = vertices[indices[i] as usize];
            let v1 = vertices[indices[i+1] as usize];
            let v2 = vertices[indices[i+2] as usize];

            let normal = normal::Normal::new([
                (v0.y()-v1.y())*(v0.z()+v1.z()) + 
                (v1.y()-v2.y())*(v1.z()+v2.z()) +
                (v2.y()-v0.y())*(v2.z()+v0.z()),
                (v0.z()-v1.z())*(v0.x()+v1.x()) +
                (v1.z()-v2.z())*(v1.x()+v2.x()) +
                (v2.z()-v0.z())*(v2.x()+v0.x()),
                (v0.x()-v1.x())*(v0.y()+v1.y()) +
                (v1.x()-v2.x())*(v1.y()+v2.y()) +
                (v2.x()-v0.x())*(v2.y()+v0.y())
            ]);

            // pushing a normal for each of the three vertices we just used
            normals.push(normal);
            normals.push(normal);
            normals.push(normal);
        }

        return normals;
    }

    // warning, beautiful code below
    // create a mesh for a sphere
    pub fn sphere() -> Self
    {
        let mut vertices = Vec::new();
        let mut indices: Indices = Vec::new();

        // simple 12 vertice sphere
        let t = (1.0+5.0_f32.sqrt())/2.0; // can't remember what this is
        vertices.push(Vertex::new([-1.0,    t,  0.0]).normalize());
        vertices.push(Vertex::new([ 1.0,    t,  0.0]).normalize());
        vertices.push(Vertex::new([-1.0,   -t,  0.0]).normalize());
        vertices.push(Vertex::new([ 1.0,   -t,  0.0]).normalize());
        vertices.push(Vertex::new([ 0.0, -1.0,    t]).normalize());
        vertices.push(Vertex::new([ 0.0,  1.0,    t]).normalize());
        vertices.push(Vertex::new([ 0.0, -1.0,   -t]).normalize());
        vertices.push(Vertex::new([ 0.0,  1.0,   -t]).normalize());
        vertices.push(Vertex::new([   t,  0.0, -1.0]).normalize());
        vertices.push(Vertex::new([   t,  0.0,  1.0]).normalize());
        vertices.push(Vertex::new([  -t,  0.0, -1.0]).normalize());
        vertices.push(Vertex::new([  -t,  0.0,  1.0]).normalize());

        // adding our triangles
        indices.extend_from_slice(&[
        00+                    0,                    00+
        02+                  9,5,0,                  00+   
        00+                    5,                    00+
        00+                                          00+
        00+                    1,                    00+
        00+                  0,  1,                  00+
        00+                7,  0,  7,                00+
        03+              7,  0, 10, 11,              00+
        00+            1,  5,  9,  5, 11,            00+
        00+          4, 11, 10,  2, 10,  7,          00+
        00+        6,  7,  1,  8,  3,  9,  4,        00+
        00+      3,  4,  2,  3,  2,  6,  3,  6,      00+
        00+    8,  3,  8,  9,  4,  9,  5,  2,  4,    00+
        04+  7,  6,  2, 10,  8,  6,  7,  9,  8,  1  +00
        ]);

        subdivide(&Mesh::create(vertices, indices, Vec::new(), "body"), 3)
    }
}

fn subdivide(mesh: &Mesh, subdivisions: usize) -> Mesh
{
    let mut vertices = mesh.vertices.clone();
    let mut indices = mesh.indices.clone();

    for _ in 0..subdivisions
    {
        let divisions: HashMap<(Indice, Indice), Indice> = HashMap::new();
        let mut new_vertices = vertices.clone();
        let mut new_indices = Vec::new();

        for num in (0..indices.len()).step_by(VERTS_PER_FACE)
        {
            // generating our new vertices
            let mut i: Vec<Indice> = Vec::new();
            for j in 0..VERTS_PER_FACE
            {
                i.push(indices[num+j]);
            }
            
            // gathering face indices
            for j in 0..VERTS_PER_FACE
            {
                let edge = (i[j], i[(j+1)%VERTS_PER_FACE]);

                // locate index if vertex was already calculated
                match divisions.get(&edge)
                {
                    Some(index) => i.push(*index),
                    None        =>
                    {
                        i.push(new_vertices.len() as Indice);
                        new_vertices.push((
                            vertices[edge.0 as usize]+
                            vertices[edge.1 as usize]
                        ).normalize());
                    }
                }
            }

            // add our new faces
            new_indices.extend_from_slice(&[
                i[0], i[3], i[5],
                i[1], i[3], i[4],
                i[2], i[4], i[5],
                i[3], i[4], i[5]
            ]);
        }

        vertices = new_vertices;
        indices = new_indices;
    }

    let normals = Mesh::calc_normals(&vertices, &indices);

    info!("mesh created {} vertices, {} indices, {} normals",
        vertices.len(),
        indices.len(),
        normals.len(),
    );

    Mesh::create(vertices, indices, normals, &mesh.shader_name)
}

impl Default for Mesh
{
    fn default() -> Self
    {
        Mesh::new()
    }
}
