use serde::{Serialize, Deserialize};
use nalgebra::{Vector3};

pub fn normalize(arr: [f32; 3]) -> [f32; 3]
{
    *Vector3::new(arr[0], arr[1], arr[2],).normalize().as_ref()
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Normal
{
    pub normal: [f32; 3]
}

impl Normal
{
    pub fn new(normal: [f32; 3]) -> Self
    {
        Self
        {
            normal: normalize(normal),
        }
    }
}

implement_vertex!(Normal, normal);
