use std::ops::{Add, Mul};
use serde::{Serialize, Deserialize};

use crate::engine::data::display::mesh::normal::normalize;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Vertex
{
    pub pos: [f32; 3],
}

impl Vertex
{
    pub fn new(pos: [f32; 3]) -> Self
    {
        Self
        {
            pos: pos,
        }
    }

    pub fn origin() -> Self
    {
        Vertex::new([0.0, 0.0, 0.0])
    }

    pub fn normalize(&self) -> Vertex
    {
        Vertex::new(normalize(self.pos))
    }

    pub const fn x(&self) -> f32
    {
        self.pos[0]
    }

    pub const fn y(&self) -> f32
    {
        self.pos[1]
    }

    pub const fn z(&self) -> f32
    {
        self.pos[2]
    }
}

impl Add for Vertex
{
    type Output = Self;

    fn add(self, other: Self) -> Self::Output
    {
        Vertex::new([
            self.x()+other.x(),
            self.y()+other.y(),
            self.z()+other.z(),
        ])
    }
}

impl Mul<f32> for Vertex {
    type Output = Self;

    fn mul(self, factor: f32) -> Self::Output
    {
        Vertex::new([
            self.x()*factor,
            self.y()*factor,
            self.z()*factor,
        ])
    }
}

impl Default for Vertex
{
    fn default() -> Self
    {
        Vertex::origin()
    }
}

implement_vertex!(Vertex, pos);
