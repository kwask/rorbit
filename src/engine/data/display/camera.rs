use std::f32::consts::PI;
use nalgebra::{Vector3, Matrix4, Point3, Perspective3};
use nalgebra as na;
use serde::{Serialize, Deserialize};

use crate::engine::types::Area;

static MAX_PITCH: f32           = PI/4.0 - PI/32.0;
static MAX_DISTANCE: f32        = 1.0e+6;
static DEFAULT_DISTANCE: f32    = 1.0e+2;
static SCALE: f32               = 1.0;
static Z_NEAR: f32              = 1.0;
static Z_FAR: f32               = 1.0e+7;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Camera
{
    pub target_id:          usize,
    pub fov:                f32,
    pub aspect_ratio:       f32,
    move_speed:             f32,
    zoom_speed:             f32,
    rotate_speed:           f32,
    mouse_sensitivity:      f32,
    scroll_sensitivity:     f32,
    zoom_scale:             f32,
    pitch:                  f32,
    yaw:                    f32,
    distance:               f32,
    front:                  Vector3<f32>,
    right:                  Vector3<f32>,
    up:                     Vector3<f32>,
}

impl Camera
{
    pub fn new(dimensions: Area, fov: f32, target_id: usize) -> Self
    {
        let mut camera = Camera 
        {
            target_id:          target_id,
            move_speed:         0.01,
            zoom_speed:         10.0,
            rotate_speed:       PI/2.0,
            mouse_sensitivity:  1.0,
            scroll_sensitivity: 0.1,
            zoom_scale:         1.0,
            aspect_ratio:       1.0,
            fov:                fov,
            pitch:              -PI/8.0,
            yaw:                0.0,
            distance:           DEFAULT_DISTANCE,
            front:              Vector3::new(0.0, 0.0, 0.0),
            right:              Vector3::new(0.0, 0.0, 0.0),
            up:                 Vector3::new(0.0, 1.0, 0.0),
        };

        camera.set_aspect_ratio(dimensions);
        camera.update_vectors();

        return camera;
    }

    pub fn set_aspect_ratio(&mut self, dimensions: Area)
    {
        self.aspect_ratio = dimensions.length/dimensions.height;
    }

    pub fn zoom(&mut self, zoom: f32)
    {
        self.distance = (DEFAULT_DISTANCE*self.zoom_scale*zoom+self.distance)
            .max(0.0)
            .min(MAX_DISTANCE);
        self.zoom_scale = self.distance/DEFAULT_DISTANCE;
    }

    pub fn zoom_scroll(&mut self, zoom: f32)
    {
        self.zoom(zoom*self.scroll_sensitivity);
    }

    pub fn rotate(&mut self, yaw: f32, pitch: f32)
    {
        self.yaw += (self.rotate_speed*yaw)%(2.0*PI);
        self.pitch = (self.rotate_speed*pitch+self.pitch)
            .max(-MAX_PITCH)
            .min(MAX_PITCH);

        self.update_vectors(); // need to update orientation vectors
    }

    pub fn rotate_mouse(&mut self, yaw: f32, pitch: f32)
    {
        self.rotate(
            yaw*self.mouse_sensitivity*self.rotate_speed,
            pitch*self.mouse_sensitivity*self.rotate_speed
        );
    }

    pub fn accelerate(&self, x: f32, y: f32, z: f32) -> Vector3<f32>
    {
        let speed = self.move_speed*self.zoom_scale;

        let mut target_vel = Vector3::new(
            self.front.index(0).clone(),
            self.front.index(1).clone(),
            0.0 // we dont want to translate on the z-axis at all
        ).normalize()*speed*x;
        target_vel += self.right*speed*y;
        target_vel += self.up*speed*z;

        return target_vel;
    }

    pub fn transformations(&self, target_pos: &Point3<f32>) -> (Matrix4<f32>, Matrix4<f32>)
    {
        // calculating our view matrix
        let offset: Vector3<f32> = self.front*self.distance*SCALE;
        let position = target_pos-offset;

        // creating our translation matrix
        let model = Matrix4::new(
            1.0, 0.0, 0.0, offset.index(0).clone(),
            0.0, 1.0, 0.0, offset.index(1).clone(),
            0.0, 0.0, 1.0, offset.index(2).clone(),
            0.0, 0.0, 0.0, 1.0
        );

        let view = na::Matrix4::look_at_rh(
            &position,        // our position
            &target_pos, // target's position
            &self.up
        );

        let projection = Perspective3::new(
            self.aspect_ratio,
            self.fov,
            Z_NEAR,
            Z_FAR
        );

        // returning our model matrix and a pre-calculated perspective matrix
        return (model, projection.as_matrix()*view);
    }

    fn update_vectors(&mut self)
    {
        let world_up = Vector3::z();

        self.front = Vector3::new(
            self.yaw.cos() * self.pitch.cos(),
            self.yaw.sin() * self.pitch.cos(),
            self.pitch.sin()
        ).normalize();

        self.right = na::Matrix::cross(&self.front, &world_up).normalize();
        self.up = na::Matrix::cross(&self.right, &self.front).normalize();
    }
}

impl Default for Camera
{
    fn default() -> Self
    {
        Camera::new(
            Area::new(800.0, 600.0),
            PI/4.0,
            0
        )
    }
}
