use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Color
{
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32
}

impl Color
{
    pub fn rgb(r: f32, g: f32, b: f32) -> Self
    {
        Self
        {
            r: r,
            g: g,
            b: b,
            a: 1.0
        }
    }

    pub fn rgba(r: f32, g: f32, b: f32, a: f32) -> Self
    {
        Self
        {
            r: r,
            g: g,
            b: b,
            a: a
        }
    }
}
