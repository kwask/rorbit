/* BASE UNITS
    Time:           s       second
    Distance:       Mm      megameters
    Mass:           Mg      megagrams
    Speed:          Mm/s    megameters per second
    Acceleration:   Mm/s^2  megameters per second squared
    Force:          MN      meganewton
*/

pub const MEGAMETER_TO_AU: f32  = 6.6845871223e-6;
pub const GRAVITATIONAL: f32    = 6.67408e-26;
pub const SOLAR_MASS: f32       = 1.9889200011446e+27;
pub const SPEED_OF_LIGHT: f32   = 299.792458;
