use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Version
{
    pub current:    String,
    pub original:   String,
}

impl Version
{
    pub fn new(version: &str) -> Self
    {
        Self
        {
            current: version.to_string(),
            original: version.to_string(),
        }
    }

    pub fn update(&mut self, version: &str)
    {
        self.current = version.to_string();
    }
}
