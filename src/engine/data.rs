use core::f32::consts::PI;
use std::collections::{HashMap, HashSet};
use std::vec::Vec;
use nalgebra::{Vector3, Point3};
#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use serde::{Serialize, Deserialize};

use crate::engine::types::Area;

pub mod constants;
pub mod display;
use display::Display;
use display::Model;
use display::mesh::Mesh;

pub mod dispatch;
use dispatch::Dispatch;

mod moveable;
pub use moveable::Moveable;

mod object;
pub use object::Object;

pub mod version;
pub use version::Version;

pub const STATE_FILE: &str = "state.json";

fn default_meshes() -> Vec<Mesh>
{
    vec![Mesh::sphere()]
}

fn default_models() -> HashMap<usize, Model>
{
    let mut models = HashMap::new();
    models.insert(0, Model::new(0));
    return models;
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Data
{
    next_object_id:     usize,
    pub version:        Version,
    pub userdata_dir:   String,
    pub resource_dir:   String,
    pub paused:         bool,
    pub time:           f32, // simulated seconds
    pub time_scale:     f32, // simulated seconds per second
    #[serde(skip)]
    pub app_name:      String,
    #[serde(skip)]
    pub should_exit:    bool,
    pub display:        Display,
    pub dispatch:       Dispatch,
    pub moveable:       Vec<Moveable>,
    pub object:         HashMap<usize, Object>,
    pub mass:           HashMap<usize, f32>,
    pub resist:         HashMap<usize, f32>,
    pub attractor:      HashSet<usize>,
    pub controllable:   Option<usize>,
    #[serde(skip, default="default_models")]
    pub models:         HashMap<usize, Model>,
    #[serde(skip, default="default_meshes")]
    pub meshes:         Vec<Mesh>,
}

impl Data
{
    pub fn new(
        app_name:       &str,
        version:        &str,
        userdata_dir:   &str,
        resource_dir:   &str,
    ) -> Self
    {
        info!("initializing...");

        let mut data = Data
        {
            next_object_id:     0,
            version:            Version::new(version),
            userdata_dir:       userdata_dir.to_string(),
            resource_dir:       resource_dir.to_string(),
            paused:             true,
            time_scale:         86400.0,
            time:               0.0,
            app_name:           app_name.to_string(),
            should_exit:        false,
            display:            Display::new(
                app_name,
                Area::new(1440.0, 900.0),
                PI/4.0,
                60.0,
                true,
                0
            ),
            dispatch:           Dispatch::default(),
            moveable:           Vec::new(),
            object:             HashMap::new(),
            mass:               HashMap::new(),
            resist:             HashMap::new(),
            attractor:          HashSet::new(),
            controllable:       None,
            models:             default_models(),
            meshes:             default_meshes(),
        };

        data.add_camera(&Point3::origin());
        data.create_solar_system();
        return data;
    }

    pub fn time_step(&self) -> f32
    {
        self.frame_time()*self.time_scale()
    }

    pub fn time_scale(&self) -> f32
    {
        self.time_scale
    }

    pub fn frame_time(&self) -> f32
    {
        self.display.framerate.s_per_frame
    }

    pub fn next_target(&mut self)
    {
        let target_id = (self.display.camera.target_id+1)%self.moveable.len();
        self.display.camera.target_id  = target_id;
    }

    pub fn previous_target(&mut self)
    {
        let target_id = if self.display.camera.target_id == 0
        {
            self.moveable.len()-1
        }else
        {
            self.display.camera.target_id-1
        };

        self.display.camera.target_id  = target_id;
    }

    pub fn get_target_id(&self) -> usize
    {
        self.display.camera.target_id
    }

    pub fn get_target(&self) -> Moveable
    {
        self.moveable[self.get_target_id()]
    }

    pub fn add_target_vel(&mut self, velocity: &Vector3<f32>)
    {
        self.set_target_vel(&(velocity+self.get_target().velocity));
    }

    pub fn set_target_vel(&mut self, velocity: &Vector3<f32>)
    {
        let target_id = self.display.camera.target_id;

        // move and take over camera if we were looking at an object
        if self.controllable != Some(target_id)
        {
            self.set_camera_pos(&self.get_target().position);    
        }

        // this is so we can't build up infinite velocity while paused
        if !self.paused
        {
            self.moveable[self.display.camera.target_id].velocity = *velocity;
        }
    }

    pub fn set_camera_pos(&mut self, position: &Point3<f32>)
    {
        let camera_id = match self.controllable
        {
            Some(id) => id,
            None =>     return,
        };

        self.moveable[camera_id].position = *position;

        // if we're setting the camera's pos, then we probably want to control it
        self.display.camera.target_id = camera_id;
    }

    pub fn add_camera(&mut self, position: &Point3<f32>)
    {
        // get existing id if we're updating an existing camera
        let camera_id = match self.controllable
        {
            Some(id) => id,
            None =>     self.get_next_object_id()
        };

        self.moveable.insert(camera_id,
            Moveable::new(
                position,
                &Vector3::new(0.0, 0.0, 0.0),
            ),
        );

        self.resist.insert(camera_id, 8.0);

        self.display.camera = display::Camera::new(
            self.display.dimensions,
            PI/4.0,
            camera_id,
        );

        self.controllable = Some(camera_id);
    }

    fn add_body(
        &mut self,
        name:       &str,
        position:   &Point3<f32>,
        velocity:   &Vector3<f32>,
        mass:       f32,
        size:       f32,
    )
    {
        let object_id = self.get_next_object_id();

        self.moveable.push(
            Moveable::new(
                position,
                velocity,
            ),
        );

        self.object.insert(
            object_id,
            Object::new(
                name,
                size,
                0,
            ),
        );

        self.mass.insert(object_id, mass);
    }

    fn add_attractor(
        &mut self,
        name:       &str,
        position:   &Point3<f32>,
        velocity:   &Vector3<f32>,
        mass:       f32,
        size:       f32,
    )
    {
        let object_id = self.get_next_object_id();

        self.moveable.push(
            Moveable::new(
                position,
                velocity,
            ),
        );

        self.object.insert(
            object_id,
            Object::new(
                name,
                size,
                0,
            ),
        );

        self.mass.insert(object_id, mass);
        self.attractor.insert(object_id);
    }

    fn get_next_object_id(&mut self) -> usize
    {
        return self.moveable.len();
    }

    fn create_solar_system(&mut self)
    {
        let earth_mass = 5.97219e+21; // in megagrams

        self.add_attractor(
            "Sol",
            &Point3::origin(),
            &Vector3::zeros(),
            1.9889200011446e+27,
            695.7,
        );

        self.add_body(
            "Mercury",
            &Point3::new(5.791e+4, 0.0, 0.0),
            &Vector3::new(0.0, 0.0474, 0.0),
            earth_mass*0.0553,
            2.4397,
        );

        self.add_body(
            "Venus",
            &Point3::new(1.08939e+5, 0.0, 0.0),
            &Vector3::new(0.0, 0.035, 0.0),
            earth_mass*0.816,
            6.052,
        );

        self.add_attractor(
            "Earth",
            &Point3::new(1.496e+5, 0.0, 0.0),
            &Vector3::new(0.0, 0.029, 0.0),
            earth_mass,
            6.37814,
        );

        self.add_body(
            "Luna",
            &Point3::new(1.496e+5+384.0000983041, 0.0, 0.0),
            &Vector3::new(0.0, 0.029+0.001, 0.0),
            earth_mass*0.0123032,
            1.7381,
        );

        self.add_body(
            "Mars",
            &Point3::new(2.279e+5, 0.0, 0.0),
            &Vector3::new(0.0, 0.024, 0.0),
            earth_mass*0.108,
            3.397,
        );

        self.add_attractor(
            "Jupiter",
            &Point3::new(7.786e+5, 0.0, 0.0),
            &Vector3::new(0.0, 0.013, 0.0),
            earth_mass*318.0,
            71.492,
        );

        self.add_body(
            "Io",
            &Point3::new(7.786e+5+421.7, 0.0, 0.0),
            &Vector3::new(0.0, 0.013+0.017334, 0.0),
            earth_mass*0.015,
            3.660/2.0,
        );

        self.add_body(
            "Europa",
            &Point3::new(7.786e+5+671.034, 0.0, 0.0),
            &Vector3::new(0.0, 0.013+0.013740, 0.0),
            earth_mass*0.008,
            3.1216/2.0,
        );

        self.add_body(
            "Ganymede",
            &Point3::new(7.786e+5+1070.412, 0.0, 0.0),
            &Vector3::new(0.0, 0.013+0.010880, 0.0),
            earth_mass*0.025,
            5.2624/2.0,
        );

        self.add_body(
            "Callisto",
            &Point3::new(7.786e+5+1882.709, 0.0, 0.0),
            &Vector3::new(0.0, 0.013+0.008204, 0.0),
            earth_mass*0.018,
            4.8206/2.0,
        );

        self.add_body(
            "Saturn",
            &Point3::new(1.433e+6, 0.0, 0.0),
            &Vector3::new(0.0, 0.0096, 0.0),
            earth_mass*95.1,
            60.268,
        );

        self.add_body(
            "Uranus",
            &Point3::new(2.873e+6, 0.0, 0.0),
            &Vector3::new(0.0, 0.0068, 0.0),
            earth_mass*14.5,
            25.559,
        );

        self.add_body(
            "Neptune",
            &Point3::new(4.495e+6, 0.0, 0.0),
            &Vector3::new(0.0, 0.0054, 0.0),
            earth_mass*17.1,
            24.764
        );

        self.add_body(
            "Pluto",
            &Point3::new(5.906e+6, 0.0, 0.0),
            &Vector3::new(0.0, 0.00474, 0.0),
            earth_mass*0.00245,
            11.85
        );
    }
}
