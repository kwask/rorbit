use std::vec::Vec;
#[allow(unused_imports)] use log::{error, warn, info, debug, trace};

use crate::engine::data::Data;

mod input;
use input::Input;

mod dispatcher;
use dispatcher::Dispatcher;

mod signaler;
use signaler::Signaler;

mod velocity;
use velocity::Velocity;

mod gravity;
use gravity::Gravity;

mod resistance;
use resistance::Resistance;

mod time;
use time::Time;

mod render;
use render::Render;

pub trait System
{
    fn name(&self) -> &'static str;
    fn stop_on_pause(&self) -> bool;
    fn init(&self, data: &mut Data) -> Result<(), String>;
    fn process(&self, data: &mut Data) -> Result<(), String>;
}

pub struct SystemManager
{
    name:           &'static str,
    pauseable:      bool,
    systems:        Vec<Box<dyn System>>,
}

impl System for SystemManager
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        self.pauseable
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        for system in &self.systems
        {
            match system.init(data)
            {
                Err(err) => error!("{:?}", err),
                _        => ()
            }
        }

        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        for system in &self.systems
        {
            if data.paused && system.stop_on_pause()
            {
                continue;
            }

            match system.process(data)
            {
                Err(err) => error!("{:?}", err),
                _        => ()
            }
        }

        Ok(())
    }
}

impl SystemManager
{
    fn new(name: &'static str, pauseable: bool) -> Self
    {
        Self
        {
            name:       name,
            systems:    Vec::new(),
            pauseable:  pauseable
        }
    }

    fn add_system(&mut self, system: Box<dyn System>)
    {
        self.systems.push(system);
    }
}

impl Default for SystemManager
{
    fn default() -> Self
    {
        let mut sm = SystemManager::new("master system manager", false);
        sm.add_system(Box::new(Input::new()));
        sm.add_system(Box::new(Dispatcher::new()));
        sm.add_system(Box::new(Signaler::new()));

        let mut physics = SystemManager::new("physics", true);
        physics.add_system(Box::new(Gravity::new()));
        physics.add_system(Box::new(Resistance::new()));
        physics.add_system(Box::new(Velocity::new()));
        physics.add_system(Box::new(Time::new()));
        sm.add_system(Box::new(physics));

        sm.add_system(Box::new(Render::new()));

        return sm;
    }
}
