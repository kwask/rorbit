#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use std::mem;

use crate::engine::types::Position;
use crate::engine::data::Data;
use crate::engine::system::System;
use crate::engine::data::dispatch::Command;

pub struct Dispatcher
{
    name: &'static str
}

impl Dispatcher
{
    pub fn new() -> Self
    {
        Self
        {
            name: "dispatcher"
        }
    }
}

impl System for Dispatcher
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        false
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        use Command::*;

        let time_step = data.frame_time();

        let mut commands = Vec::new();
        mem::swap(&mut commands, &mut data.dispatch.commands);

        for command in commands
        {
            match command
            {
                WindowResize(size)   => data.display.resize_window(size),
                MoveCursor(position) => 
                {
                    data.dispatch.cursor_velocity = Position::new(
                        position.x-data.dispatch.cursor.x,
                        position.y-data.dispatch.cursor.y,
                    );
                    data.dispatch.cursor = position;
                },
                Zoom(zoom)      => data.display.camera.zoom(time_step*zoom),
                CycleRenderMode => data.display.cycle_display_mode(),
                NextTarget      => data.next_target(),
                PreviousTarget  => data.previous_target(),
                GrabCamera      => data.display.capture_mouse(true),
                UngrabCamera    => data.display.capture_mouse(false),
                TogglePause     => data.paused = !data.paused,
                Exit            => data.should_exit = true,
            }
        }

        Ok(())
    }
}

