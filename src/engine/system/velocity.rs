#[allow(unused_imports)] use log::{error, warn, info, debug, trace};

use crate::engine::data::Data;
use crate::engine::system::System;

pub struct Velocity
{
    name: &'static str
}

impl Velocity
{
    pub fn new() -> Self
    {
        Self
        {
            name: "velocity"
        }
    }
}

impl System for Velocity
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        true
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        let time_step = data.time_step();
        
        // updating position from velocity
        for moveable in &mut data.moveable
        {
            moveable.position = moveable.position+(moveable.velocity*time_step);
        }

        Ok(())
    }
}
