use crate::engine::data::Data;
use crate::engine::system::System;

pub struct Time
{
    name: &'static str
}

impl Time
{
    pub fn new() -> Self
    {
        Self
        {
            name: "time"
        }
    }
}

impl System for Time
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        true
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        data.time += data.time_step();

        Ok(())
    }
}
