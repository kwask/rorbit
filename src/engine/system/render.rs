use std::fs::{File, read_dir};
use std::path::Path;
use std::io::Read;
#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use gl::Surface;
use gl_text::{FontTexture, TextDisplay};
use nalgebra::Matrix4;

use crate::engine::data::Data;
use crate::engine::system::System;
use crate::engine::data::display::{SHADER_DIR, FONT_DIR, MeshBuffer};

#[derive(Copy, Clone, Debug)]
struct Position
{
    pub world_pos:   [f32; 3],
    pub scale:       f32,
}

implement_vertex!(Position, world_pos, scale);

#[derive(Clone, Debug)]
struct ShaderFile
{
    pub vertex:     String,
    pub fragment:   String,
}

fn create_shader(
    shader_files: ShaderFile,
    data: &Data
) -> Result<gl::Program, std::io::Error>
{
    let mut vert = String::new();
    File::open(shader_files.vertex.clone())?.read_to_string(&mut vert)?;
    let mut frag = String::new();
    File::open(shader_files.fragment.clone())?.read_to_string(&mut frag)?;

    Ok(gl::Program::from_source(
        &data.display.display,
        &vert,
        &frag,
        None
    ).unwrap())
}

pub struct Render
{
    name:       &'static str,
}

impl Render
{
    pub fn new() -> Self
    {
        Self
        {
            name: "render"
        }
    }
}

impl System for Render
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        false
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        info!("compiling shaders");
        let directory_path = (data.resource_dir.clone()+SHADER_DIR).to_string();
        let shader_dir = Path::new(&directory_path);
        if !shader_dir.exists() || !shader_dir.is_dir() 
        {
            return Err("shader directory does not exist".to_string());
        }

        // 1. iterate through each file in the shader dir
        // 2. collect shader files related to selected
        // 3. compile all related shader files into a program
        for file in read_dir(shader_dir).map_err(|_| {
            "could not read directory"
        })?
        {
            let file = match file
            {
                Ok(f) => f,
                _ => continue,
            };

            let path = file.path();
            if path.is_dir()
            {
                continue;
            }

            let name: String = match path.file_stem()
            {
                Some(name) => match name.to_str()
                {
                    Some(name) => name.to_string(),
                    _ => continue,
                },
                None => continue 
            };

            if data.display.shaders.contains_key(&name)
            {
                continue;
            }

            let shader_files = ShaderFile
            {
                vertex:     (directory_path.clone()+&name+".vert").to_string(),
                fragment:   (directory_path.clone()+&name+".frag").to_string(),
            };

            match create_shader(shader_files, data)
            {
                Ok(shader) =>
                {
                    data.display.shaders.insert(&name.clone(), shader);
                    info!("shader program {:?} created", name.clone());
                },
                Err(err)    =>
                {
                    return Err(format!(
                        "shader program {:?} failed to compile: {}",
                        name.clone(),
                        err
                    ));
                }
            }
        }

        info!("generating mesh buffers");
        for mesh in &data.meshes
        {
            data.display.mesh_buffers.push(MeshBuffer::new(
                gl::VertexBuffer::new(
                    &data.display.display,
                    &mesh.vertices
                ).unwrap(),
                gl::IndexBuffer::new(
                    &data.display.display,
                    gl::index::PrimitiveType::TrianglesList,
                    &mesh.indices
                ).unwrap(),
                gl::VertexBuffer::new(
                    &data.display.display,
                    &mesh.normals
                ).unwrap(),
                data.display.shaders.id(&mesh.shader_name)
            ));
        }

        info!("loading fonts");
        let directory = data.resource_dir.clone()+FONT_DIR+"BebasNeue-Regular.ttf";
        let font = FontTexture::new(
            &data.display.display,
            File::open(Path::new(&directory)).unwrap(),
            256,
            FontTexture::ascii_character_list()
        ).unwrap();
        data.display.fonts.insert("BebasNeue-Regular", font);

        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        let mut scene = data.display.display.draw();
        
        // clearing the scene
        let clear = &data.display.clear_color;
        scene.clear_color_and_depth((clear.r, clear.g, clear.b, clear.a), 1.0);

        // generating per-instances mappings
        let per_instance = {
            let pos_data = data.object.iter().map(|entry|
            {
                Position
                {
                    world_pos:  data.moveable[*entry.0].position.coords.into(),
                    scale:      entry.1.scale,
                }
            }).collect::<Vec<_>>();

            gl::vertex::VertexBuffer::dynamic(
                &data.display.display,
                &pos_data
            ).unwrap()
        };

        // generating our transformation matrices
        let (model, perspective) = data.display.camera.transformations(
            &data.get_target().position
        );

        // passing the data to the display device to be rendered
        let parameters = &gl::DrawParameters {
            depth: gl::Depth {
                test: gl::draw_parameters::DepthTest::IfLess,
                write: true,
                .. Default::default()
            },
            .. Default::default()
        };

        let mesh_id = 0; // eventually we'll have more than 1 mesh
        let mesh_buffer = &data.display.mesh_buffers[mesh_id];
        scene.draw(
            (
                &data.display.mesh_buffers[mesh_id].vertices,
                per_instance.per_instance().unwrap()
            ),
            &mesh_buffer.indices,
            &data.display.shaders.get(mesh_buffer.shader),
            &uniform!
            {
                perspective: *perspective.as_ref(),
                model: *model.as_ref()
            },
            parameters,
        ).unwrap();

        // display our target name
        if let Some(object) = data.object.get(&data.get_target_id())
        {
            let text = TextDisplay::new(
                &data.display.text_system,
                data.display.fonts.get(0),
                &object.name
            );
            let size = (1.0/text.get_height()).min(1.0/text.get_width());

            let factor_w = size*250.0/data.display.dimensions.length;
            let factor_h = size*80.0/data.display.dimensions.height;

            let padding = 0.05;
            let scale = Matrix4::new(
                factor_w,      0.0,    0.0,   -1.0+padding,
                     0.0, factor_h,    0.0,    1.0-factor_h-padding,
                     0.0,      0.0,    1.0,    0.0,
                     0.0,      0.0,    0.0,    1.0
            );

            gl_text::draw(
                &text,
                &data.display.text_system,
                &mut scene,
                *scale.as_ref(),
                (1.0, 1.0, 0.0, 1.0)
            ).unwrap();
        }

        // displaying our finished scene
        scene.finish().unwrap();

        Ok(())
    }
}
