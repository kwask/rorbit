#[allow(unused_imports)] use log::{error, warn, info, debug, trace};
use std::borrow::Borrow;

use crate::engine::types::Position;
use crate::engine::data::Data;
use crate::engine::system::System;
use crate::engine::data::dispatch::Signal;

pub struct Signaler
{
    name: &'static str
}

impl Signaler
{
    pub fn new() -> Self
    {
        Self
        {
            name: "signaler"
        }
    }
}

impl System for Signaler
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        false
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        use Signal::*;

        let time_step = data.frame_time();

        for signal in data.dispatch.signals.clone()
        {
            match signal
            {
                Move(x, y, z) => data.add_target_vel(
                    &data.display.camera.accelerate(
                        time_step*(x as f32),
                        time_step*(y as f32),
                        time_step*(z as f32)
                    )
                ),
                Rotate(yaw, pitch) => data.display.camera.rotate(
                    time_step*(yaw as f32),
                    time_step*(pitch as f32
                )),
                CameraGrabbed => 
                {
                    let dimensions = data.display.dimensions;
                    let cursor_velocity = data.dispatch.cursor_velocity;

                    // we dont need to time scale these values since they're
                    // already scaled to a per-iteration value
                    data.display.camera.rotate_mouse(
                        -(cursor_velocity.x/dimensions.length),
                        -(cursor_velocity.y/dimensions.height),
                    );

                    // resetting our cursor to the center of the screen
                    let center = Position::new(
                        dimensions.length/2.0,
                        dimensions.height/2.0,
                    );

                    data.display.display
                        .gl_window()
                        .borrow()
                        .window()
                        .set_cursor_position(center.into())?;

                    data.dispatch.cursor = center;
                    data.dispatch.cursor_velocity = Position::new(0.0, 0.0);
                }
            }
        }

        Ok(())
    }
}

