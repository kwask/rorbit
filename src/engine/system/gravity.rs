#[allow(unused_imports)] use log::{error, warn, info, debug, trace};

use crate::engine::data::{Data, constants};
use crate::engine::system::System;

pub struct Gravity
{
    name: &'static str
}

impl Gravity
{
    pub fn new() -> Self
    {
        Self
        {
            name: "gravity"
        }
    }
}

impl System for Gravity
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        true
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        let time_step = data.time_step();

        for id in &data.attractor
        {
            let grav_factor = data.mass[id]*constants::GRAVITATIONAL*time_step;
            let grav_pos = data.moveable[*id];

            // remove ourself from the list of massive objects
            let mut mass = data.mass.clone();
            mass.remove(id);

            // gravitational attraction on anything with mass
            for (id, _) in &mass
            {
                let moveable = &mut data.moveable[*id];
                let deltav_gravity = (moveable.position-grav_pos.position)
                    .normalize()*grav_factor/nalgebra::distance_squared(
                    &moveable.position,
                    &grav_pos.position
                );

                // gravity is an attractive force, so we subtract the acceleration
                moveable.velocity = moveable.velocity-deltav_gravity;
            }
        }

        Ok(())
    }
}
