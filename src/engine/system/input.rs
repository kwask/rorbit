#[allow(unused_imports)] use log::{error, warn, info, debug, trace};

use crate::engine::data::Data;
use crate::engine::data::dispatch::{Button, Command};
use crate::engine::system::System;

use glut;

pub struct Input
{
    name: &'static str
}

impl Input
{
    pub fn new() -> Self
    {
        Self
        {
            name: "input"
        }
    }
}

impl System for Input
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        false
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        use glut::WindowEvent::{*};
        use glut::ElementState::{Pressed, Released};
        use Command::*;

        // clone to avoid double borrow with data
        let mut dispatch = data.dispatch.clone();

        data.display.event_pump.poll_events(|system_event|
        {
            match system_event
            {
                glut::Event::WindowEvent { event, .. } => 
                {
                    match event
                    {
                        CloseRequested => dispatch.queue(Exit),
                        Resized (size) => dispatch.queue(WindowResize(size.into())),
                        KeyboardInput { input, .. } => match input.virtual_keycode
                        {
                            Some(key) => 
                            {
                                let button = Button::Key(key);
                                match input.state
                                {
                                    Pressed => dispatch.press(&button),
                                    Released => dispatch.release(&button)
                                }
                            },
                            None => (),
                        },
                        MouseInput { state, button, ..} => 
                        {
                            let button = Button::Mouse(button);
                            match state
                            {
                                Pressed => dispatch.press(&button),
                                Released => dispatch.release(&button)
                            }
                        },
                        CursorMoved { position, .. } => dispatch.queue(
                            MoveCursor(position.into())
                        ),
                        MouseWheel { delta, .. } =>
                        {
                            use glut::MouseScrollDelta::{LineDelta, PixelDelta};

                            let zoom = match delta
                            {
                                LineDelta (_, y) => -y,
                                PixelDelta(position) => -(position.y as f32),
                            };

                            dispatch.queue(
                                Zoom(zoom)
                            );
                        },
                        _   => (),
                    }
                },
                _ => (),
            }
        });

        data.dispatch = dispatch;

        Ok(())
    }
}

