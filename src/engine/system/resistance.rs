#[allow(unused_imports)] use log::{error, warn, info, debug, trace};

use crate::engine::data::Data;
use crate::engine::system::System;

pub struct Resistance
{
    name: &'static str
}

impl Resistance
{
    pub fn new() -> Self
    {
        Self
        {
            name: "resistance"
        }
    }
}

impl System for Resistance
{
    fn name(&self) -> &'static str
    {
        self.name
    }

    fn stop_on_pause(&self) -> bool
    {
        true
    }

    fn init(&self, data: &mut Data) -> Result<(), String>
    {
        Ok(())
    }

    fn process(&self, data: &mut Data) -> Result<(), String>
    {
        let time_step = data.frame_time();

        // lowering velocity of anything in resist
        for(id, resistance) in &data.resist
        {
            let moveable = &mut data.moveable[*id];
            moveable.velocity -= moveable.velocity*(*resistance)*time_step;
        }
        
        Ok(())
    }
}
