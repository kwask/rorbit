#[macro_use] extern crate gl;

mod engine;

fn main()
{
    let mut engine: engine::Engine = engine::Engine::new(
        "Rorbit",
        "userdata/",
        "resources/",
    );
    
    while !engine.exited()
    {
        engine.transition();
    }
}
